import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  LoadingController,
  ToastController
} from '@ionic/angular';
import { AuthService } from './../services/auth.service';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  userData: any;

  constructor(
    private auth: AuthService,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private route: Router,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    this.loadingCtrl
      .create({
        message: 'Getting ready...'
      })
      .then(loadingEl => {
        loadingEl.present();
        this.auth
          .show('user', localStorage.getItem('userID'))
          .subscribe(resData => {
            console.log(resData['data'][0]);
            this.userData = resData['data'][0];
            loadingEl.dismiss();
          });
      });
  }


   // Sharing app
   share() {
    const title = 'JustFreddies Social Sharing';
    const text = 'Experience the best pizza you`ve ever had right at your fingertips, anywhere, anytime.';
    const url = 'https://play.google.com/store/apps/details?id=com.phalcontectincgh.justfreddies';
    this.socialSharing.share(text, title, '', url).then(() => {
    });
  }

  // Codes for displaying about us information
  about() {
    this.alertCtrl
      .create({
        header: 'JustFreddies®',
        message: `
        <p>JustFreddies® is application that brings you closer to the best Pizza Joint Ghana and beyond.</p>
        <p>This application was designed and developed with ❤️ by Phalcon Tech Inc Gh</p>
        <p text-center><strong>Version 1.0</strong></p>
      `,
        mode: 'ios',
        buttons: ['Okay']
      })
      .then(alertEl => {
        alertEl.present();
      });
  }


  faqs() {
    this.alertCtrl
      .create({
        header: 'JustFreddies®',
        message: `<p>FAQs would be added soon. Our team is working to answer all questions immediately.</p>`,
        mode: 'ios',
        buttons: ['Okay']
      })
      .then(alertEl => {
        alertEl.present();
      });
  }


  logout() {
    this.alertCtrl
      .create({
        header: 'Exit Application',
        message: 'Are you sure about closing the app?',
        mode: 'ios',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'Yes',
            handler: () => {
              this.signout();
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }

  signout() {
    this.loadingCtrl
      .create({
        message: 'Logging out...',
        duration: 2000
      })
      .then(loadingEl => {
        loadingEl.present();
        localStorage.clear();
        this.route.navigateByUrl('/signin');
      });
  }


}
