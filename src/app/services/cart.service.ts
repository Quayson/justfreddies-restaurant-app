import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private data = [
    {
      category: 'Pizza Box',
      expanded: true,
      products: [
        {
          id: '358',
          name: 'Samore Toure Delicacy-Meduim',
          description:
            'Curry Chicken, Red and White Onions, Fresh Tomatoes and Peppers',
          price: '30',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Samore-Toure-Delicacy.png'
        },
        {
          id: '359',
          name: 'Hannibal Fiery-Meduim',
          description: 'Spicy Pork, Pineapple, Chili Sauce, Peppers and Onions',
          price: '30',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Hannibal-Fiery.png'
        },
        {
          id: '362',
          name: 'Mansa Musa Feast-Meduim',
          description:
            'Mincemeat, Spicy Chicken, Spicy Pork, Peppers and Onions',
          price: '35',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Mansa-Musa-Feast.png'
        },
        {
          id: '363',
          name: 'Yaa Asantewaa Delectable-Meduim',
          description:
            'Spinach, Tomato, Red and White Onions, Chili Sauce and Kpakpo Shito',
          price: '30',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Yaa-Asantewaa-Delectable.png'
        },
        {
          id: '364',
          name: 'Shaka Zulu Fiesta-Meduim',
          description: 'Honey Glazed Pork, Mixed Peppers, Aubergine and Onions',
          price: '30',
          image: ''
        },
        {
          id: '365',
          name: 'Nefertiti Deluxe-Meduim',
          description: 'Tuna, Olives and Mixed Vegetables',
          price: '35',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nefertiti-Deluxe.png'
        },
        {
          id: '366',
          name: 'Amina`s Gourmet-Meduim',
          description: '',
          price: '35',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Aminaâ€™s-Gourmet.png'
        },
        {
          id: '368',
          name: 'Nzinga Passion-EXLarge',
          description: 'Pepperoni, Double Mozzarella and Fresh Tomato',
          price: '50',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nzinga-Passion.png'
        },
        {
          id: '371',
          name: 'Makeeda\'s Margherita-Medium',
          description:
            'Rich Tomato Sauce, Double Mozzarella, Basil and Fresh Tomato',
          price: '30',
          image: ''
        },
        {
          id: '372',
          name: 'Osei Tutu\'s Golden Delight-Meduim',
          description: 'Spicy Meatballs,Â  Sausage and Bacon with Vegetables',
          price: '35',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Osei-Tutuâ€™s-Golden-Delight.png'
        },
        {
          id: '417',
          name: 'Makeeda\'s Margherita-Large',
          description:
            'Rich Tomato Sauce, Double Mozzarella, Basil and Fresh Tomato',
          price: '40',
          image: ''
        },
        {
          id: '418',
          name: 'Makeeda\'s Margherita-EXLarge',
          description:
            'Rich Tomato Sauce, Double Mozzarella, Basil and Fresh Tomato',
          price: '50',
          image: ''
        },
        {
          id: '442',
          name: 'Osei Tutu\'s Golden Delight-Large',
          description: 'Spicy Meatballs,Â  Sausage and Bacon with Vegetables',
          price: '45',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Osei-Tutuâ€™s-Golden-Delight.png'
        },
        {
          id: '445',
          name: 'Osei Tutu\'s Golden Delight-EXLarge',
          description: 'Spicy Meatballs,Â  Sausage and Bacon with Vegetables',
          price: '55',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Osei-Tutuâ€™s-Golden-Delight.png'
        },
        {
          id: '446',
          name: 'Nefertiti Deluxe-Large',
          description: 'Tuna, Olives and Mixed Vegetables',
          price: '45',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nefertiti-Deluxe.png'
        },
        {
          id: '447',
          name: 'Nefertiti Deluxe-EXLarge',
          description: 'Tuna, Olives and Mixed Vegetables',
          price: '55',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nefertiti-Deluxe.png'
        },
        {
          id: '448',
          name: 'Nzinga Passion-Large',
          description: 'Pepperoni, Double Mozzarella and Fresh Tomato',
          price: '40',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nzinga-Passion.png'
        },
        {
          id: '449',
          name: 'Nzinga Passion-Meduim',
          description: 'Pepperoni, Double Mozzarella and Fresh Tomato',
          price: '30',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Nzinga-Passion.png'
        },
        {
          id: '452',
          name: 'Aminaâ€™s Gourmet-EXLarge',
          description: '',
          price: '55',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Aminaâ€™s-Gourmet.png'
        },
        {
          id: '453',
          name: 'Aminaâ€™s Gourmet-Large',
          description: '',
          price: '45',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Aminaâ€™s-Gourmet.png'
        },
        {
          id: '455',
          name: 'Samore Toure Delicacy-Large',
          description:
            'Curry Chicken, Red and White Onions, Fresh Tomatoes and Peppers',
          price: '40',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Samore-Toure-Delicacy.png'
        },
        {
          id: '456',
          name: 'Samore Toure Delicacy-EXLarge',
          description:
            'Curry Chicken, Red and White Onions, Fresh Tomatoes and Peppers',
          price: '50',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Samore-Toure-Delicacy.png'
        },
        {
          id: '458',
          name: 'Hannibal Fiery-Large',
          description: 'Spicy Pork, Pineapple, Chili Sauce, Peppers and Onions',
          price: '40',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Hannibal-Fiery.png'
        },
        {
          id: '459',
          name: 'Hannibal Fiery-EXLarge',
          description: 'Spicy Pork, Pineapple, Chili Sauce, Peppers and Onions',
          price: '50',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Hannibal-Fiery.png'
        },
        {
          id: '461',
          name: 'Mansa Musa Feast-Large',
          description:
            'Mincemeat, Spicy Chicken, Spicy Pork, Peppers and Onions',
          price: '45',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Mansa-Musa-Feast.png'
        },
        {
          id: '462',
          name: 'Mansa Musa Feast-EXLarge',
          description:
            'Mincemeat, Spicy Chicken, Spicy Pork, Peppers and Onions',
          price: '55',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Mansa-Musa-Feast.png'
        },
        {
          id: '464',
          name: 'Yaa Asantewaa Delectable-Large',
          description:
            'Spinach, Tomato, Red and White Onions, Chili Sauce and Kpakpo Shito',
          price: '40',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Yaa-Asantewaa-Delectable.png'
        },
        {
          id: '465',
          name: 'Yaa Asantewaa Delectable-EXLarge',
          description:
            'Spinach, Tomato, Red and White Onions, Chili Sauce and Kpakpo Shito',
          price: '50',
          image:
            'https://pizzabox.justfreddies.com/wp-content/uploads/2019/05/freddies_Pizza_Yaa-Asantewaa-Delectable.png'
        },
        {
          id: '467',
          name: 'Shaka Zulu Fiesta-Large',
          description: 'Honey Glazed Pork, Mixed Peppers, Aubergine and Onions',
          price: '40',
          image: ''
        },
        {
          id: '468',
          name: 'Shaka Zulu Fiesta-EXLarge',
          description: 'Honey Glazed Pork, Mixed Peppers, Aubergine and Onions',
          price: '50',
          image: ''
        },
        {
          id: '470',
          name: 'Uncle Freddies Special-EXLarge',
          description: 'Spicy Pork, Mince Meat, Bacon, Pepperoni and Sausage',
          price: '55',
          image: ''
        },
        {
          id: '472',
          name: 'Uncle Freddies Special-Medium',
          description: 'Spicy Pork, Mince Meat, Bacon, Pepperoni and Sausage',
          price: '35',
          image: ''
        },
        {
          id: '474',
          name: 'Uncle Freddies Special-Large',
          description: 'Spicy Pork, Mince Meat, Bacon, Pepperoni and Sausage',
          price: '46',
          image: ''
        }
      ]
    },
    {
      category: 'Restaurant',
      products: [
        {
          id: '26',
          name: 'Vegetable Fried Rice & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/vegetable-fried-rice-and-chicken.jpg'
        },
        {
          id: '28',
          name: 'Jollof & Grilled Tilapia',
          price: '23',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jolloff-and-tillapia.jpg'
        },
        {
          id: '29',
          name: 'Jollof & Grilled Pork',
          price: '21',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-pork.jpg'
        },
        {
          id: '31',
          name: 'Jollof & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Chicken-and-Jollof-rice.jpg'
        },
        {
          id: '32',
          name: 'Jollof & Fish Kebabs',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-and-fish-khebabs.jpg'
        },
        {
          id: '33',
          name: 'Jollof & Beef Sauce',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Jolofbeef-sauce.jpg'
        },
        {
          id: '35',
          name: 'Vegetable Fried Rice & Grilled Tilapia',
          price: '23',
          image: ''
        },
        {
          id: '36',
          name: 'Vegetable Fried Rice & Grilled Pork',
          price: '21',
          image: ''
        },
        {
          id: '37',
          name: 'Vegetable Fried Rice & Grilled Chicken',
          price: '22',
          image: ''
        },
        {
          id: '38',
          name: 'Vegetable Fried Rice & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-16.jpg'
        },
        {
          id: '39',
          name: 'Vegetable Fried Rice & Fish Kebabs',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/fish-kebab2.jpg'
        },
        {
          id: '40',
          name: 'Vegetable Fried Rice & Beef Sauce',
          price: '25',
          image: ''
        },
        {
          id: '41',
          name: 'Vegetable Fried Rice & Chicken Sauce',
          price: '25',
          image: ''
        },
        {
          id: '42',
          name: 'Banku & Grilled Tilapia',
          price: '23',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-7.jpg'
        },
        { id: '43', name: 'Banku & Grilled Pork', price: '21', image: '' },
        { id: '44', name: 'Banku & Grilled Chicken', price: '22', image: '' },
        { id: '45', name: 'Banku & Fried Chicken', price: '22', image: '' },
        { id: '46', name: 'Banku & Fish Kebabs', price: '25', image: '' },
        { id: '47', name: 'Banku & Beef Sauce', price: '25', image: '' },
        { id: '48', name: 'Banku & Chicken Sauce', price: '25', image: '' },
        {
          id: '49',
          name: 'Fried Yam & Grilled Tilapia',
          price: '23',
          image: ''
        },
        { id: '50', name: 'Fried Yam & Grilled Pork', price: '21', image: '' },
        {
          id: '51',
          name: 'Fried Yam & Grilled Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-12.jpg'
        },
        { id: '52', name: 'Fried Yam & Fried Chicken', price: '22', image: '' },
        { id: '53', name: 'Fried Yam & Fish Kebabs', price: '25', image: '' },
        { id: '54', name: 'Fried Yam & Beef Sauce', price: '25', image: '' },
        {
          id: '9052',
          name: 'Vegetable Fried Rice & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/vegetable-fried-rice-and-chicken.jpg'
        },
        {
          id: '9053',
          name: 'Beef Burger & Chips',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/beef-burger-and-chips.jpeg'
        },
        {
          id: '9054',
          name: 'Jollof & Grilled Tilapia',
          price: '23',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jolloff-and-tillapia.jpg'
        },
        {
          id: '9055',
          name: 'Jollof & Grilled Pork',
          price: '21',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-pork.jpg'
        },
        {
          id: '9056',
          name: 'Jollof & Grilled Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Jollof-and-grill-.jpg'
        },
        {
          id: '9057',
          name: 'Jollof & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Chicken-and-Jollof-rice.jpg'
        },
        {
          id: '9058',
          name: 'Jollof & Fish Kebabs',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-and-fish-khebabs.jpg'
        },
        {
          id: '9059',
          name: 'Jollof & Beef Sauce',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Jolofbeef-sauce.jpg'
        },
        {
          id: '9060',
          name: 'Jollof & Chicken Sauce',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-chicken-source.jpg'
        },
        {
          id: '9061',
          name: 'Vegetable Fried Rice & Grilled Tilapia',
          price: '23',
          image: ''
        },
        {
          id: '9062',
          name: 'Vegetable Fried Rice & Grilled Pork',
          price: '21',
          image: ''
        },
        {
          id: '9063',
          name: 'Vegetable Fried Rice & Grilled Chicken',
          price: '22',
          image: ''
        },
        {
          id: '9064',
          name: 'Vegetable Fried Rice & Fried Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-16.jpg'
        },
        {
          id: '9065',
          name: 'Vegetable Fried Rice & Fish Kebabs',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/fish-kebab2.jpg'
        },
        {
          id: '9066',
          name: 'Vegetable Fried Rice & Beef Sauce',
          price: '25',
          image: ''
        },
        {
          id: '9067',
          name: 'Vegetable Fried Rice & Chicken Sauce',
          price: '25',
          image: ''
        },
        {
          id: '9068',
          name: 'Banku & Grilled Tilapia',
          price: '23',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-7.jpg'
        },
        { id: '9069', name: 'Banku & Grilled Pork', price: '21', image: '' },
        { id: '9070', name: 'Banku & Grilled Chicken', price: '22', image: '' },
        { id: '9071', name: 'Banku & Fried Chicken', price: '22', image: '' },
        { id: '9072', name: 'Banku & Fish Kebabs', price: '25', image: '' },
        { id: '9073', name: 'Banku & Beef Sauce', price: '25', image: '' },
        { id: '9074', name: 'Banku & Chicken Sauce', price: '25', image: '' },
        {
          id: '9075',
          name: 'Fried Yam & Grilled Tilapia',
          price: '23',
          image: ''
        },
        {
          id: '9076',
          name: 'Fried Yam & Grilled Pork',
          price: '21',
          image: ''
        },
        {
          id: '9077',
          name: 'Fried Yam & Grilled Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-12.jpg'
        },
        {
          id: '9078',
          name: 'Fried Yam & Fried Chicken',
          price: '22',
          image: ''
        },
        { id: '9079', name: 'Fried Yam & Fish Kebabs', price: '25', image: '' },
        { id: '9080', name: 'Fried Yam & Beef Sauce', price: '25', image: '' },
        {
          id: '9081',
          name: 'Fried Yam & Chicken Sauce',
          price: '25',
          image: ''
        },
        {
          id: '9082',
          name: 'Potato Chips & Grilled Tilapia',
          price: '23',
          image: ''
        },
        {
          id: '9083',
          name: 'Potato Chips & Grilled Pork',
          price: '21',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/potato-chips-with-pork-grilled.jpg'
        },
        {
          id: '9084',
          name: 'Potato Chips & Grilled Chicken',
          price: '22',
          image: ''
        },
        {
          id: '9085',
          name: 'Potato Chips & Fried Chicken',
          price: '22',
          image: ''
        },
        {
          id: '9086',
          name: 'Potato Chips & Fish Kebabs',
          price: '25',
          image: ''
        },
        {
          id: '9087',
          name: 'Potato Chips & Beef Sauce',
          price: '25',
          image: ''
        },
        {
          id: '9088',
          name: 'Potato Chips & Chicken Sauce',
          price: '25',
          image: ''
        },
        {
          id: '9089',
          name: 'Braised Rice & Grilled Tilapia',
          price: '23',
          image: ''
        },
        {
          id: '9090',
          name: 'Braised Rice & Grilled Pork',
          price: '21',
          image: ''
        },
        {
          id: '9091',
          name: 'Braised Rice & Grilled Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-22.jpg'
        },
        {
          id: '9092',
          name: 'Braised Rice & Fried Chicken',
          price: '22',
          image: ''
        },
        {
          id: '9093',
          name: 'Braised Rice & Fish Kebabs',
          price: '25',
          image: ''
        },
        {
          id: '9094',
          name: 'Braised Rice & Beef Sauce',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-20.jpg'
        },
        {
          id: '9095',
          name: 'Braised Rice & Chicken Sauce',
          price: '25',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/JustfreddiesA-21.jpg'
        },
        {
          id: '9096',
          name: 'Jollof & Grilled Chicken(MBF)',
          price: '13',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Jollof-and-grill-.jpg'
        },
        {
          id: '9097',
          name: 'Jollof & Fried Chicken(MBF)',
          price: '13',
          image: ''
        },
        {
          id: '9098',
          name: 'Fried Rice & Grilled Chicken',
          price: '22',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/fried-rice-and-grilled-chiken.jpg'
        },
        {
          id: '9099',
          name: 'Fried Rice & Fried Chicken(MBF)',
          price: '13',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Fried-Rice-and-fried-chicken.jpg'
        },
        {
          id: '9100',
          name: 'Potato Chips & Fried Chicken(MBF)',
          price: '13',
          image: ''
        },
        { id: '9101', name: 'Chicken(MBF)', price: '13', image: '' },
        {
          id: '9102',
          name: 'Fried Yam',
          price: '7',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/fried-yam.jpg'
        },
        {
          id: '9103',
          name: 'Fried Potato',
          price: '5',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/fried-Potato.jpg'
        },
        {
          id: '9104',
          name: 'Jollof Rice',
          price: '8',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/jollof-rice.jpg'
        },
        {
          id: '9105',
          name: 'Fried Rice',
          price: '8',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Fried-Rice.jpg'
        },
        {
          id: '9106',
          name: 'Quarter Chicken',
          price: '14',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/chicken-quarter.jpg'
        },
        {
          id: '9107',
          name: 'Black Peper',
          price: '2',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/IMG_2096.jpg'
        },
        {
          id: '9108',
          name: 'Red Peper',
          price: '2',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/Red-Peper.jpg'
        },
        {
          id: '9109',
          name: 'Green Peper (Kpakpo Shito)',
          price: '2',
          image:
            'https://www.justfreddies.com/wp-content/uploads/2018/07/green-Peper-sauce.jpg'
        }
      ]
    }
  ];

  private cart = [];

  constructor() {}

  // Get all products in system
  getProducts() {
    return this.data;
  }

  // Get all items in Cart
  getCart() {
    return this.cart;
  }


  // Clear everything in cart
  clearCart() {
    this.cart = [];
    console.log('Cart: ' + this.cart);
  }

  // Sending all selected products to cart
  addProduct(product) {
    this.cart.push(product);
  }

  // Removing products from Cart
  removeProduct(name) {
    // for (const i in this.cart) {
    //   if (this.cart[i].name === name) {
    //     this.cart[i].count --;
    //     if (this.cart[i].count === 0) {
    //       this.cart.splice(i, 1);
    //     }
    //   }
    // }
  }

  // Deleting from Cart too
  deleteFromCart(i) {
    console.log(i);
    if (i === 0) {
      this.cart.shift();
    } else {
      this.cart.splice(i, 1);
      console.log(this.cart);
    }
  }
}
