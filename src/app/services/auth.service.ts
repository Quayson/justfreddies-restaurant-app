import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const server = 'http://41.211.11.229:8005/api/';
// const server = 'http://127.0.0.1:8000/ap/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // setting token as variable
  token = localStorage.getItem('token');

  // user data as array
  // private _userDetails = new BehaviorSubject<[]><[]>;

  constructor( private http: HttpClient) {}

  // index of a resource
  get(url) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url, { headers: config });
  }

  // store a new resource
  store(url, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.post(server + url, payload, { headers: config });
  }

  // show a single resource
  show(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url + '/' + id, { headers: config });
  }

  // show edit details for  a single resource
  edit(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.get(server + url + '/' + id, { headers: config });
  }

  // update a single resource
  update(url, id, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.patch(server + url + '/' + id, payload, {
      headers: config
    });
  }

  // delete a particular resource
  destroy(url, id) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    config.append('Authorization', 'Bearer ' + this.token);
    return this.http.delete(server + url + '/' + id, { headers: config });
  }

  // registration of users
  createUser(url, payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    return this.http.post(server + url, payload, { headers: config });
  }

  // authentication for user login
  authenticate(payload) {
    const config = new HttpHeaders();
    config.append('Accept', 'application/json');
    return this.http.post('http://41.211.11.229:8005/oauth/token', payload, {
      headers: config
    });
  }

   // getting access token
   getToken() {
    return localStorage.getItem('token');
  }

  // function to check user type (either customer or staff)
  userData() {
    return JSON.parse(localStorage.getItem('user'));
  }


  // check user login
  isLoggedIn() {
    return this.getToken() !== null && this.userData() !== null;
  }
}
