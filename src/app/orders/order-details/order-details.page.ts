import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {

  isLoading = false;

  orderSub: Subscription;
  order: any;
  meals: any;
  isOrdable = false;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      // console.log(paramMap.get('orderID'));
      if (!paramMap.has('orderID')) {
        this.navCtrl.navigateBack('/orders');
        return;
      }
      this.isLoading = true;
      this.orderSub = this.auth.show('order', paramMap.get('orderID')).subscribe(
          response => {
            this.order = response['order'];
            this.meals = response['order']['order'];
            this.isOrdable = this.order.status === 'Complete';
            this.isLoading = false;
          },
          error => {
            this.alertCtrl
              .create({
                header: 'Error Detected.',
                message: 'Order data could not be found',
                buttons: [
                  {
                    text: 'Okay',
                    handler: () => {
                      this.router.navigate(['orders']);
                    }
                  }
                ]
              })
              .then(alertEl => {
                alertEl.present();
              });
          }
        );
    });
  }

  reOrder() {
    console.log(this.order);
  }

}
