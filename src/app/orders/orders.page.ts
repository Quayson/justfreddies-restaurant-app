import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription } from 'rxjs';

import { AuthService } from './../services/auth.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  order: any;
  allOrders: any;
  cancelledOrders: any;
  listedOrders: any;
  lastestOrder: any;
  isLoading = false;
  private orderSub: Subscription;
  listedLoadedOrders = []; // same as listedLoadedPlaces
  relevantOrders = [];  // Same as relevantPlaces

  constructor(private auth: AuthService, private menuCtrl: MenuController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.auth.show('user_orders', localStorage.getItem('userID')).subscribe(
      response => {
        this.isLoading = false;
        this.allOrders = response['data'];
        // console.log(this.allOrders);
        this.relevantOrders = this.allOrders;
        this.listedLoadedOrders = response['data'];
        this.getCancelledOrders();
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  getCancelledOrders() {
    this.auth
      .show('user_cancelled_orders', localStorage.getItem('userID'))
      .subscribe(
        response => {
          this.cancelledOrders = response['data'];
        },
        error => {
          console.log(error);
          this.isLoading = false;
        }
      );
  }

  onOpenMenu() {
    this.menuCtrl.toggle();
  }

  onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    if (event.detail.value === 'all') {
      this.relevantOrders = this.allOrders;
      this.listedLoadedOrders = this.relevantOrders.slice(1);
    } else {
      this.relevantOrders = this.cancelledOrders;
      this.listedLoadedOrders = this.relevantOrders.slice(1);
    }
  }

}
