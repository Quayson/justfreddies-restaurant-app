import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  NavController,
  MenuController,
  ToastController,
  AlertController,
  LoadingController,
  Events
} from '@ionic/angular';
import { AuthService } from './../services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss']
})
export class SigninPage implements OnInit {

  public loginForm: FormGroup;

  loginData = {
    username: '',
    password: '',
    grant_type: 'password',
    client_id: '2',
    client_secret: 'P9ygg3NCt5rqGQA16QOryCG5UAwOFlU0pwnvzFIY',
    scope: '*'
  };

  isLoggin = false;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private route: Router,
    private auth: AuthService,
    private events: Events
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ],
      password: [
        null,
        Validators.compose([Validators.required, Validators.minLength(5)])
      ]
    });
  }

  buildData() {
    this.loginData.username = this.loginForm.value.email;
    this.loginData.password = this.loginForm.value.password;
  }

  signin() {
    this.isLoggin = true;
    this.buildData();
    this.auth.authenticate(this.loginData).subscribe(
      response => {
        // console.log(response);

        if (response !== null || response !== undefined) {
          localStorage.setItem('token', response['access_token']);
          setTimeout(() => {
            this.checkAccessLevel(this.loginData);
            this.isLoggin = false;
          }, 5000);
        }
      },
      error => {
        if (error.status === 500) {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message: 'Internal error, please contact Tech support',
              duration: 2000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        } else if (error.status === 401) {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message: 'Unauthorized credentials, please signup',
              position: 'top',
              duration: 2000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        } else {
          this.isLoggin = false;
          this.toastCtrl
            .create({
              message: 'Operation unsuccessful;, please try again later.',
              duration: 2000,
              showCloseButton: true
            })
            .then(toastEl => {
              toastEl.present();
            });
        }
      }
    );
  }

  checkAccessLevel(data) {
    this.auth.store('access_level', data).subscribe(
      response => {
        // console.log('User Details : ' + JSON.stringify(response));
        let access_level = response['user']['access_level'];
        localStorage.setItem('user', JSON.stringify(response['user']));
        localStorage.setItem('userID', response['user']['id']);
        localStorage.setItem('username', response['user']['surname']);
        localStorage.setItem('userType', response['user']['access_level']);

        // send user data to events
        // this.events.publish('user', response['user']);

        if (access_level === 'Customer') {
          localStorage.setItem('access_level', 'Customer');
          this.toastCtrl
            .create({
              message: 'Welcome ' + localStorage.getItem('username'),
              position: 'top',
              duration: 1500
            })
            .then(toastEl => {
              this.navCtrl.navigateRoot('dashboard');
              toastEl.present();
            });
        } else {
          localStorage.setItem('access_level', 'Contractor');
          this.toastCtrl
            .create({
              message: 'Welcome ' + localStorage.getItem('username'),
              duration: 2000,
              position: 'top'
            })
            .then(toastEl => {
              this.navCtrl.navigateRoot('con-dashboard');
              toastEl.present();
            });
        }
      },
      error => {
        console.log(error);
        this.alertCtrl
          .create({
            message:
              'Signing In process could not be complete, please contact tech support.',
            buttons: ['Okay']
          })
          .then(alertEl => {
            alertEl.present();
          });
      }
    );
  }
}
