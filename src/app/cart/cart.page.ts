import { Router } from '@angular/router';
import { Orders } from './../interfaces/orders';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './../services/auth.service';
import {
  IonItemSliding,
  LoadingController,
  AlertController,
  ToastController,
} from '@ionic/angular';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss']
})
export class CartPage implements OnInit {

  orderForm: FormGroup;

  orders: Orders;

  selectedItems = [];

  total = 0;

  cartData = {
    user_id: localStorage.getItem('userID'),
    order: [],
    estimated_cost: this.total,
    location: '',
    status: 'Awaiting Delivery'
  };

  constructor(
    private cartService: CartService,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private loadingCtel: LoadingController,
    private router: Router,
    private toastCtrl: ToastController) {
    this.orderForm = this.formBuilder.group({
      location: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    const items = this.cartService.getCart();
    console.log(items);
    const selected = {};
    for (const obj of items) {
      if (selected[obj.id]) {
        selected[obj.id].count++;
      } else {
        selected[obj.id] = { ...obj, count: 1 };
      }
    }
    this.selectedItems = Object.keys(selected).map(key => selected[key]);
    this.total = this.selectedItems.reduce((a, b) => a + b.count * b.price, 0);

  }


  // Clear Cart
  clearCart() {
    this.alertCtrl.create({
      mode: 'ios',
      header: 'Clearing Cart',
      message: 'Are you sure about clearing your cart?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Yes',
          handler: () => {
            this.cartCleared();
          }
        }
      ]
    }).then(alertEL => {
      alertEL.present();
    });
  }


  // Function for clearing Cart
  cartCleared() {
    this.cartService.clearCart();
    const items = this.cartService.getCart();
    // console.log(items);
    const selected = {};
    for (const obj of items) {
      if (selected[obj.id]) {
        selected[obj.id].count++;
      } else {
        selected[obj.id] = { ...obj, count: 1 };
      }
    }
    this.selectedItems = Object.keys(selected).map(key => selected[key]);
    this.total = this.selectedItems.reduce((a, b) => a + b.count * b.price, 0);
    this.router.navigateByUrl('dashboard');
  }

  // Removing item from Cart
  onDeleteItem(i, slidinEl: IonItemSliding) {
    slidinEl.close();
    console.log(i);
    const index = this.selectedItems.indexOf(i);
    if (index > -1) {
      this.cartService.deleteFromCart(index);
      this.selectedItems.splice(index, -1);
      console.log(this.selectedItems);
    }
    this.total = this.selectedItems.reduce((a, b) => a + b.count * b.price, 0);
  }



  // Building Data for user order
  buildData() {
    this.cartData.order = this.selectedItems;
    this.cartData.estimated_cost = this.total;
    this.cartData.location = this.orderForm.value.location;
  }

  checkout() {
    this.alertCtrl.create({
      header: 'Notice',
      message: `We are currently operating the Cash on Delivery system.
       New payment methods would be added to the app on our next update. Cheers!!!`,
       mode: 'ios',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Okay',
          handler: () => {
            this.makeOrder();
          }
        }
      ]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  makeOrder() {
    this.buildData();
    console.log(this.cartData);
    this.loadingCtel.create({
      message: `Placing your order, please wait...`
    }).then(loadingEl => {
      loadingEl.present();
      this.auth.store('order', this.cartData).subscribe(response => {
        loadingEl.dismiss();
        this.toastCtrl.create({
          message: response['message'],
          duration: 2500,
          position: 'top'
        }).then(toastEl => {
          toastEl.present();
          this.cartCleared();
        });
      }, error => {
        console.log(error);
        loadingEl.dismiss();
        this.toastCtrl.create({
          message: 'Error placing your order, please try again later',
          duration: 2500,
          position: 'top'
        }).then(toastEl => {
          toastEl.present();
        });
      });
    });
  }
}
