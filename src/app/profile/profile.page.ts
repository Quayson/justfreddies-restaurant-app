import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  NavController,
  LoadingController,
  ToastController
} from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

   // Variables declaration
   public editForm: FormGroup;

   userData: any;

   editData = {
     // user_id: '',
     email: '',
     phone: ''
   };

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private auth: AuthService) {
      this.editForm = this.formBuilder.group({
        surname: [
          '',
          Validators.required,
        ],
        othernames: ['', Validators.required],
        email: [
          null,
          Validators.compose([
            Validators.required,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ])
        ],
        phone: [
          null,
          Validators.compose([Validators.required, Validators.minLength(10)])
        ]
      });
    }

    ngOnInit() {
      this.getDetails();
    }

    buildData() {
      this.editData.email = this.editForm.value.email;
      this.editData.phone = this.editForm.value.phone;
    }

    getDetails() {
      this.loadingCtrl
        .create({
          message: 'Getting form ready...',
          spinner: 'crescent'
        })
        .then(loadingEl => {
          loadingEl.present();
          this.auth
            .show('user', localStorage.getItem('userID'))
            .subscribe(resData => {
              console.log(resData['data'][0]);
              this.userData = resData['data'][0];
              this.prepopulateForm(this.userData);
              loadingEl.dismiss();
            });
        });
    }

    prepopulateForm(data) {
      this.editForm.get('surname').setValue(this.userData.surname);
      this.editForm.get('othernames').setValue(this.userData.othernames);
      this.editForm.get('email').setValue(this.userData.email);
      this.editForm.get('phone').setValue(this.userData.phone);
    }

    editProfile() {
      this.buildData();
      console.log(this.editData, localStorage.getItem('userID'));
      this.loadingCtrl
        .create({
          message: 'Updating Profile...',
          duration: 3000,
          spinner: 'crescent'
        })
        .then(loadingEl => {
          loadingEl.present();
          this.auth.update('user', localStorage.getItem('userID'), this.editData).subscribe(resData => {
            console.log(resData);
            this.toastCtrl
            .create({
              message: resData['message'],
              position: 'top',
              duration: 3000
            })
            .then(toastEl => {
              toastEl.present();
              loadingEl.dismiss();
            });
            this.navCtrl.navigateForward('/dashboard');
          });
        });
    }

}
