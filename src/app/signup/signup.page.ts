import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import {
  NavController,
  LoadingController,
  ToastController
} from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage implements OnInit {

  public registerForm: FormGroup;

  isLoading = false;

  regData = {
    surname: '',
    othernames: '',
    email: '',
    phone: '',
    access_level: 'Customer',
    password: ''
  };

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService,
    private toastCtrl: ToastController
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      surname: [null, Validators.compose([Validators.required])],
      othernames: [null, Validators.compose([Validators.required])],
      phone: [
        null,
        Validators.compose([Validators.required, Validators.minLength(9)])
      ],
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ],
      user_type: [null, Validators.required],
      password: [
        null,
        Validators.compose([Validators.required, Validators.minLength(5)])
      ]
    });
  }

  buildData() {
    this.regData.surname = this.registerForm.value.surname;
    this.regData.othernames = this.registerForm.value.othernames;
    this.regData.email = this.registerForm.value.email;
    this.regData.phone = this.registerForm.value.phone;
    this.regData.password = this.registerForm.value.password;
  }

  signinPage() {
    this.router.navigateByUrl('signin');
  }

  signup() {
    this.isLoading = true;
    this.buildData();
    console.log(this.regData);
    this.loadingCtrl
      .create({
        message: 'Creating user profile. Please wait...'
      })
      .then(loadingEl => {
        loadingEl.present();
        this.auth.createUser('user', this.regData).subscribe(
          response => {
            console.log(response);
            if (response !== null || response !== undefined) {
              loadingEl.dismiss();
              this.isLoading = false;
              this.toastCtrl
                .create({
                  message: response['message'],
                  showCloseButton: true,
                  position: 'top',
                  keyboardClose: true
                })
                .then(toastEl => {
                  toastEl.present();
                });
              this.router.navigate(['signin']);
            }
          },
          error => {
            if (error.status === 500) {
              loadingEl.dismiss();
              this.isLoading = false;
              this.toastCtrl
                .create({
                  message: 'Please check form data, email might already exist.',
                  duration: 3000,
                  showCloseButton: true
                })
                .then(toastEl => {
                  toastEl.present();
                });
            } else {
              loadingEl.dismiss();
              this.isLoading = false;
              this.toastCtrl
                .create({
                  message: 'Operation unsuccessful;, please try again later.',
                  duration: 3000,
                  showCloseButton: true
                })
                .then(toastEl => {
                  toastEl.present();
              });
            }
          }
        );
      });
  }
}
