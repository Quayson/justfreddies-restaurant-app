import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from './../services/cart.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {
  // Placeholder image
  imgPlaceholder = '../../assets/images/logos/logo1.png';

  // Variable declarations
  cart = [];
  items = [];

  // Slider Configuration
  sliderConfig = {
    slidersPerview: 1.6,
    spaceBetween: 10,
    centeredSlides: true
  };

  constructor(private router: Router, private cartService: CartService) {}

  ngOnInit() {
    this.items = this.cartService.getProducts();
    this.cart = this.cartService.getCart();
  }

  // Add products to cart
  addToCart(product) {
    this.cartService.addProduct(product);
  }

  // Click to open cart
  openCart() {
    this.router.navigate(['cart']);
  }

  // On removing item from cart
  deleteFromCart(name) {
    console.log(name);
    this.cartService.removeProduct(name);
  }
}
