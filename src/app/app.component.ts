import { Router } from '@angular/router';
import { Component } from '@angular/core';

import { Platform, AlertController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  editProfile() {
    this.router.navigateByUrl('/edit-profile');
  }

  workStatus() {
    console.log('Work status changed.');
  }


  logout() {
    this.alertCtrl.create({
      header: 'Exit Application',
      message: 'Are you sure about closing the app?',
      mode: 'ios',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondaary'
        },
        {
          text: 'Yes',
          handler: () => {
            this.signout();
          }
        }
      ]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  signout() {
    this.loadingCtrl.create({
      message: 'Logging out...',
      duration: 2000
    }).then(loadingEl => {
      loadingEl.present();
      localStorage.clear();
      this.router.navigateByUrl('/signin');
    });
  }
}
