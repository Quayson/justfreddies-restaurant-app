import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { trigger, transition, style, state, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.page.html',
  styleUrls: ['./tutorials.page.scss'],
  animations: [
    trigger('bounce', [
          state('*', style({
              transform: 'translateX(0)'
          })),
          transition('* => rightSwipe', animate('700ms ease-out', keyframes([
            style({transform: 'translateX(0)', offset: 0}),
            style({transform: 'translateX(-65px)',  offset: 0.3}),
            style({transform: 'translateX(0)',     offset: 1.0})
          ]))),
          transition('* => leftSwipe', animate('700ms ease-out', keyframes([
            style({transform: 'translateX(0)', offset: 0}),
            style({transform: 'translateX(65px)',  offset: 0.3}),
            style({transform: 'translateX(0)',     offset: 1.0})
          ])))
      ])
    ]
})
export class TutorialsPage implements OnInit {

  // @ViewChild(Slides) slides: Slides;
  @ViewChild(IonSlides, {static: false}) slides: IonSlides;

  skipMsg = 'Next';
  state = 'x';

  constructor(private router: Router) { }

  ngOnInit() {
  }


  skip() {
    this.router.navigateByUrl('signin');
  }

  slideChanged() {
    if (this.slides.isEnd()) {
      this.skipMsg = 'Alright, I got it';
    }
  }

  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex()) {
      this.state = 'rightSwipe';
    } else {
      this.state = 'leftSwipe';
    }
  }

  animationDone() {
    this.state = 'x';
  }


}
