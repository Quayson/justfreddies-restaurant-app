import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {

  subject: string;
  body: string;
  email: any;

  constructor(
    private callNumber: CallNumber,
    private emailComposer: EmailComposer
    ) { }

  ngOnInit() {
  }

  makeCall() {
    this.callNumber.callNumber(`0201187201`, true).then(
      data => {
      },
      error => {
        alert(JSON.stringify(error));
      }
    );
  }

  sendEmail() {
    console.log(this.subject, this.body);
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        // Now we know we can send
      } else {
        alert('Your device does not support sending email.');
      }
    });

    this.emailComposer.open({
      to: 'godfred109@gmail.com',
      cc: 'akrongwork@gmail.com',
      subject: 'JustFreddies Tech Support: ' + this.subject,
      body: this.body,
    });
  }

}
