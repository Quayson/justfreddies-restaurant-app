import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TutorialGuard } from './guards/tutorial.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'app',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'signin', loadChildren: './signin/signin.module#SigninPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'cart',
    loadChildren: './cart/cart.module#CartPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfilePageModule'
  },
  {
    path: 'orders',
    loadChildren: './orders/orders.module#OrdersPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'intro',
    loadChildren: './intro/intro.module#IntroPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'order-details/:orderID',
    loadChildren:
      './orders/order-details/order-details.module#OrderDetailsPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'support',
    loadChildren: './support/support.module#SupportPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'tutorials',
    loadChildren: './tutorials/tutorials.module#TutorialsPageModule'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
